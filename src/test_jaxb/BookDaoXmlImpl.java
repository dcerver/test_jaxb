package test_jaxb;

import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BookDaoXmlImpl {
	// POJO: "Plain Old Java Object"
	private Book pojo;

	public BookDaoXmlImpl() {
		super();
	}
	
	public BookDaoXmlImpl(Book pojo) throws Exception {
		super();
		this.pojo = pojo;
		marshallPojo(this);
	}
	public Book getPojo() {
		return pojo;
	}
	public void setPojo(Book pojo) {
		this.pojo =  pojo;
	}
	
	public static void marshallPojo(BookDaoXmlImpl dao) throws Exception {
		// Guardamos cada objeto según su ID, que será único (para que no hayan ficheros con el mismo nombre)
	    JAXBContext contextObj = JAXBContext.newInstance(BookDaoXmlImpl.class);  
	    Marshaller marshallerObj = contextObj.createMarshaller();
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshallerObj.marshal(dao, new FileOutputStream(Integer.toString(dao.getPojo().getId()).concat(".xml")));
	}
}
