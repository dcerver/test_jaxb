package test_jaxb;

public class BookDaoFactory {
    public static BookDaoXmlImpl getDao(Book book) throws Exception { 
    	return new BookDaoXmlImpl(book);
    	// Cuando tengamos las dos implementaciones:
        //if (Main.implementation.equals("xml")) {
        //    return new BookDaoXmlImpl();
        //} else {
        //    return new BookDaoSqlImpl();
        //}
    }
}
