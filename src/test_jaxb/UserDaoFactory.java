package test_jaxb;

public class UserDaoFactory {
    public static UserDaoXmlImpl getDao(User user) throws Exception { 
    	return new UserDaoXmlImpl(user);
    	// Cuando tengamos las dos implementaciones:
        //if (Main.implementation.equals("xml")) {
        //    return new UserDaoXmlImpl();
        //} else {
        //    return new UserDaoSqlImpl();
        //}
    }
}