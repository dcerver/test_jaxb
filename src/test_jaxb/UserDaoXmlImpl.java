package test_jaxb;

import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserDaoXmlImpl {
	// POJO: "Plain Old Java Object"
	private User pojo;

	public UserDaoXmlImpl() {
		super();
	}
		
	public UserDaoXmlImpl(User pojo) throws Exception {
		super();
		this.pojo = pojo;
		marshallPojo(this);
	}
	public User getPojo() {
		return pojo;
	}
	public void setPojo(User pojo) {
		this.pojo =  pojo;
	}
	
	public static void marshallPojo(UserDaoXmlImpl dao) throws Exception {
		// Guardamos cada objeto según su ID, que será único (para que no hayan ficheros con el mismo nombre)
		JAXBContext contextObj = JAXBContext.newInstance(UserDaoXmlImpl.class);  
	    Marshaller marshallerObj = contextObj.createMarshaller();
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshallerObj.marshal(dao, new FileOutputStream(Integer.toString(dao.getPojo().getId()).concat(".xml")));
	}
}
