package test_jaxb;

public class Main {
	
	// Escoger "xml" o "mysql" 
	public static String implementation = "xml";

	public static void main(String[] args) throws Exception {

		System.out.println("Inicio Programa");
		Book book1 = new Book(33, "El Quijote");
		BookDaoXmlImpl book1Dao = BookDaoFactory.getDao(book1);
		User user1 = new User(43, "Carlos Garcia");
		UserDaoXmlImpl user1Dao = UserDaoFactory.getDao(user1);
		System.out.println("Fin Programa");
	}
}
